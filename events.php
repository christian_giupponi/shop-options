<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * NitroCart    NitroCart.net - A full featured shopping cart system for PyroCMS
 *
 * @author      Salvatore Bordonaro
 * @version     2.2.0.2050
 * @website     http://nitrocart.net
 *              http://www.inspiredgroup.com.au
 *
 * @system      PyroCMS 2.2.x
 *
 */
class Events_Shop_Options
{

	protected $ci;

	public $mod_details = array(
			      'name'=> 'Options', //Label of the module
			      'namespace'=>'shop_options',
			      'product-tab'=> TRUE, //This is to tell the core that we want a tab
			      'product-tab-order'=> 1, //This is to tell the core that we want a tab
			      'cart'=> TRUE, //this is to be hooked up with the core cart process
			      'has_admin'=> TRUE,
	);

	/**
	 * Get the CI instance into this object
	 *
	 * @param unknown_type $var
	 */
	public function __get($var)
	{
		if (isset(get_instance()->$var))
		{
			return get_instance()->$var;
		}
	}

	// Put code here for everywhere
	public function __construct()
	{


		//New events to replace all of the above -
		Events::register('SHOPEVT_AdminProductGet', array($this, 'shopevt_admin_product_get'));
		Events::register('SHOPEVT_AdminProductSave', array($this, 'shopevt_admin_product_save'));
		Events::register('SHOPEVT_AdminProductDelete', array($this, 'shopevt_admin_product_delete'));
		Events::register('SHOPEVT_AdminProductDuplicate', array($this, 'shopevt_admin_product_duplicate'));

		//cart price updating
		Events::register('SHOPEVT_BeforeCartItemAdded', array($this, 'shopevt_BeforeCartItemAdded'));



		//Store Front event
		Events::register('SHOPEVT_GetVariances', array($this, 'shopevt_get_variances'));

	}

	/**
	 * This is where we will catch any options selecte for the product and rweturn them.
	 * We do not need to do anything on the update as options cant be changed for the update
	 */
	public function shopevt_BeforeCartItemAdded($data)
	{
		$input = $this->input->post();

		$this->load->model('shop_options/options_m');
		$this->load->library('shop_options/Options_library');

		foreach($input as $key => $value)
		{
			//is this an option field
			if( $this->startsWith($key, 'option_') )
			{
				$new_key = str_ireplace('option_', '', $key );

				//Get the label from the server

				$_option = $this->options_m->get($new_key);

				if($_option)
				{
					//Now we want to get the text version of the value, using the value as a key
					//$new_value = $this->options_library->get_key_value( $new_key, $value );

					//$value = ($new_value)? $new_value : $value ;

					$data->product['options'][$_option->name] = $value;

				}
				else
					$data->product['options'][$new_key] = $value;

			}
		}

		//var_dump($this->input->post());
		//var_dump($data);die;


	}

	private function startsWith($string_n, $startsWith)
	{
		if( substr( $string_n, 0, strlen($startsWith) ) === $startsWith )
			return TRUE;
		else
			return FALSE;
	}





	public function shopevt_get_variances($in_object)
	{


	}



	public function shopevt_admin_product_delete($deleted_product_id)
	{
		$this->load->model('shop_options/options_admin_m');
		$this->options_admin_m->delete_by_product( $deleted_product_id );
	}

	/**
	 * Copy a product and its relevant contents
	 *
	 * @param  array  $duplicateData [description]
	 * @return [type]                [description]
	 */
	public function shopevt_admin_product_duplicate($duplicateData = array())
	{
		$or_id  = $duplicateData['OriginalProduct'];
		$new_id = $duplicateData['NewProduct'];

		$this->load->model('shop_options/options_admin_m');
		$this->options_admin_m->duplicate( $or_id ,$new_id );
	}



	/**
	 * This will be called when the admin product data has been requested.
	 * It will inform all other modules to fetch any data that may be associated
	 * The ID of the product is passed (always by ID and Never by SLUG)
	 */
	public function shopevt_admin_product_get($product)
	{
		// Send data back
		$product->module_tabs[] = (object) $this->mod_details;


		$this->load->model('shop_options/options_admin_m','options_admin_m');

  		$the_list = (array)$this->group_m->get_all() ;

		$options = $this->options_admin_m->getOptionsByProduct($product);
		$pyroUserGroups = array_for_select( $the_list ,'id', 'description');

		$product->modules['shop_options']=array();
		$product->modules['shop_options']['groups'] = $pyroUserGroups; //select box of pyro user groups
		$product->modules['shop_options']['options'] = $options; //array of options in DB
	}


}
/* End of file events.php */