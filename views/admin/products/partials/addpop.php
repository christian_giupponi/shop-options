<div class='shop_options_popup'>
				<div class="discounts_message_area">

				</div>
				<fieldset>

							<div class="value">
								<h3>Create New Option for this product</h3>
							</div>

							<div class="value">
								Name<span>*</span><br />
								<input type='text' name='option_name' value='' placeholder='Name: Colour'>
							</div>

							<div class="value">
								Form Type<br />
								<select name='option_type'>
									<option value='select'>Select List</option>
									<option value='radio'>Radio List</option>
									<option value='text'>Text</option>
								</select>

							</div>

							<div class="value">
								Label<br />
								<input type='text' name='option_label' value='' placeholder='The label to display'>
								<input type='hidden' name='option_order' value='0'>
								<input type='hidden' name='option_default' value='0'>
								<input type='hidden' name='option_enabled' value='1'>

							</div>

							<div class="value">
								List values<br />
								<textarea name='option_values' placeholder='Blue,Red,Green  (Comma seperated items), Only used for list options' style='width: 300px;'></textarea>
							</div>


							<a class="addOption button">Add</a>

				</fieldset>
</div>