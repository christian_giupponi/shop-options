				<div class="options_message_area">

				</div>

				<fieldset>

						<h3>
							Options
						</h3>

						<?php if($userDisplayMode == 'edit'):?>
							<a class="sbtn glow modal" href="admin/shop_options/options/addpop/<?php echo $id;?>">Add option</a>
						<?php endif;?>

				</fieldset>
				<fieldset>
					<h3>Options assigned to this Product</h3>
					<?php if(isset($modules['shop_options']['groups'])): ?>
						<?php if(isset($modules['shop_options']['options'])): ?>
								<table class="shop_options_list">
									<tr>
										<th>ID</th>
										<th>Name</th>
										<th>Options</th>
										<th>Type</th>
										<?php if($userDisplayMode == 'edit'):?>
											<th>Action</th>
										<?php endif;?>
									</tr>
									<?php foreach($modules['shop_options']['options'] AS $option): ?>
										<tr option-id='<?php echo $option->id; ?>'>

											<td><?php echo $option->id; ?></td>
											<td><?php echo $option->name;?></td>
											<td><?php echo $option->values; ?> </td>
											<td><?php echo $option->type; ?> </td>
											<?php if($userDisplayMode == 'edit'):?>
												<td><a href="#" class='delOption sbtn sbtn-flat red'>Delete</a></td>
											<?php endif;?>
										</tr>
									<?php endforeach; ?>
								</table>
						<?php endif; ?>
					<?php endif; ?>

				</fieldset>


<?php if($userDisplayMode == 'edit'):?>

	<script>



	function addOption(obj)
	{
		$('.shop_options_popup').html('Option has been added.');

		var str  ="<tr option-id='"+obj.option_id+"'>";
		   str += "<td>"+ obj.option_id +"</td>";
		   str += "<td>"+ obj.name +"</td>";
		   str += "<td>"+ obj.values +" </td>";
		   str += "<td>"+ obj.type +"</td>";
		   str += "<td><a href='#' class='delOption sbtn sbtn-flat red'>Delete</a></td></tr>";

		$('table.shop_options_list tbody').append(str);

	}


	$(function() {


		//http://stackoverflow.com/questions/16362778/running-jquery-function-on-dynamically-created-content
		//$( ".addDiscount" ).on('click',function() {
		$(document).on('click', '.addOption', function(event) {

			/*
			option_type,

			option_label,
			option_order,
			option_default,
			option_enabled,
			option_values
			*/


		        var otype 	= $("select[name='option_type']").val();
		        var oname 	= $("input[name='option_name']").val();
		        var olabel 	= $("input[name='option_label']").val();
		        var ovalues = $("textarea[name='option_values']").val();


	            var url = "<?php echo site_url();?>admin/shop_options/options/add/<?php echo $id;?>/";
	            var senddata =
	            {
	            	product_id:<?php echo $id;?>,
					type:otype,
					name:oname,
					label:olabel,
					values:ovalues,
					enabled:1,
					default:0,
					order:0
	            };

	            $.post(url,senddata).done(function(data)
	            {

	                var obj = jQuery.parseJSON(data);

	                if(obj.status == 'success')
	                {
	                	addOption(obj);
	                }
	                else
	                {
	                	alert("Failed:"+ obj.message);
	                }

	            });

				//$(".options_message_area").html(mymessage);

		        // Prevent Navigation
		        event.preventDefault();

		  });



		//$( ".delDiscount" ).click(function( event ) { //does not work with dynamic
		$(document).on('click', '.delOption', function(event) {	//this works much better


		        var options = $(this).parent().parent();

		        //Warn about delete
		        if(confirm("Are you sure you want to remove this options ? "))
		        {
		            var url = "<?php echo site_url();?>admin/shop_options/options/remove/" + options.attr("option-id");
					var mymessage = '';
		            $.post(url).done(function(data)
		            {

		                var obj = jQuery.parseJSON(data);

		                if(obj.status == 'success')
		                {
		                	//http://stackoverflow.com/questions/3655627/jquery-append-object-remove-it-with-delay
		                	//http://stackoverflow.com/questions/8100772/why-cant-i-delay-a-remove-call-with-jquery
							options.fadeTo("slow", 0.1);
							setTimeout(function() {
							  	options.delay(3000).remove();
		                    	mymessage= "Success";
							}, 2000);
		                }
		                else
		                {
		                	 alert("Failed");
		                }

		            });

					$(".options_message_area").html(mymessage);
		            $(".options_message_area").delay(2000).fadeTo("slow", 0.6).remove();

		        }

		        // Prevent Navigation
		        event.preventDefault();


		  });


		}); //end query
	</script>
<?php endif; ?>