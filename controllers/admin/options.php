<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * NitroCart    NitroCart.net - A full featured shopping cart system for PyroCMS
 *
 * @author      Salvatore Bordonaro
 * @version     2.2.0.2050
 * @website     http://nitrocart.net
 *              http://www.inspiredgroup.com.au
 *
 * @system      PyroCMS 2.2.x
 *
 */
class Options extends Admin_Controller
{

	protected $section = 'options';
	private $data;


	public function __construct()
	{
		parent::__construct();
		role_or_die('shop_options', 'admin_options');
	}

	public function index(){}



	public function addpop($product_id)
	{

		$this->load->model('shop_options/options_admin_m');

		$this->template
				->set_layout(FALSE)
				->set('jsexec', "$('.tooltip-s').tipsy()")
				->build('admin/products/partials/addpop');

	}

	public function add($product_id)
	{

		$return_array = $this->getAjaxReturnObject();

		if($input = $this->input->post())
		{

			$this->load->model('shop_options/options_admin_m');


			if($disc = $this->options_admin_m->create($input))
			{
				$disc['status'] = 'success';
			}
			else
			{
				$disc['message'] = "Failed to add.";
			}

			$this->sendAjaxReturnObject($disc);

		}
	}

	public function remove($id=0)
	{

		$return_array = $this->getAjaxReturnObject();

		$this->load->model('shop_options/options_admin_m');

		if($this->options_admin_m->delete($id))
		{
			$return_array['status'] = 'success';
		}


		$this->sendAjaxReturnObject($return_array);
	}


	private function getAjaxReturnObject()
	{
		$ret_array = array();
		$ret_array['status'] = 'error';
		$ret_array['message'] = '';

		return $ret_array;
	}

	private function sendAjaxReturnObject($array_object)
	{
		echo json_encode($array_object);die;
	}

}