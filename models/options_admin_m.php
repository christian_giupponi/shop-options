<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * NitroCart    NitroCart.net - A full featured shopping cart system for PyroCMS
 *
 * @author      Salvatore Bordonaro
 * @version     2.2.0.2050
 * @website     http://nitrocart.net
 *              http://www.inspiredgroup.com.au
 *
 * @system      PyroCMS 2.2.x
 *
 */
class Options_admin_m extends MY_Model {


	public $_table = 'shop_options';


	public function __construct()
	{
		parent::__construct();
	}

	public function getOptionsByProduct($product)
	{
		return $this->where('product_id',$product->id)->get_all();
	}



	public function create($inputs)
	{


		$to_insert = array(
				'product_id' => (int)$inputs['product_id'],
				'name' => $inputs['name'],
				'label' => $inputs['label'],
				'type' => $inputs['type'],
				'values' => $inputs['values'],
				'order' => 0,
				'default' => 0,
				'enabled' => 1, //(int) $inputs['enabled'],
		);

		$this->insert($to_insert); //returns id

		$to_insert['option_id'] = $this->db->insert_id();

		return $to_insert;
	}

	public function duplicate( $or_id , $new_id )
	{

		//fetch all rows where prod id = $or_id
		$original_product_options = $this->where('product_id',$or_id)->get_all();

		foreach($original_product_options AS $option)
		{
			//create the input
			$to_insert = array(
				'product_id' => $new_id,
				'name' => $option->name,
				'label' => $option->label,
				'type' =>  $option->type,
				'values' =>  $option->values,
				'order' =>  $option->order,
				'default' =>  $option->default,
				'enabled' =>  $option->enabled,
			);

			//Add record
			$this->insert($to_insert); //returns id

		}

		return TRUE;
	}

	/*delete the record*/
	public function delete_by_product( $product_id )
	{

		$items_to_delete = $this->where('product_id',$product_id)->get_all();

		foreach($items_to_delete AS $option)
		{
			$this->delete($option->id);
		}

		return TRUE;
	}


}