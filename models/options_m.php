<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * NitroCart    NitroCart.net - A full featured shopping cart system for PyroCMS
 *
 * @author      Salvatore Bordonaro
 * @version     2.2.0.2050
 * @website     http://nitrocart.net
 *              http://www.inspiredgroup.com.au
 *
 * @system      PyroCMS 2.2.x
 *
 */
class Options_m extends MY_Model {


	public $_table = 'shop_options';
	protected $i_exist = FALSE;
	public function __construct()
	{
		parent::__construct();
	}


	public function getOptionsByProduct( $product_id )
	{
		$result = $this->where('enabled',1)->where('product_id',$product_id)->order_by('order','asc')->get_all();

		return $result;
	}
}