<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * NitroCart    NitroCart.net - A full featured shopping cart system for PyroCMS
 *
 * @author      Salvatore Bordonaro
 * @version     2.2.0.2050
 * @website     http://nitrocart.net
 *              http://www.inspiredgroup.com.au
 *
 * @system      PyroCMS 2.2.x
 *
 */
class Plugin_Shop_Options extends Plugin
{


	public $version = '1.0.0';
	public $name = array(
		'en' => 'Shop Options',
	);
	public $description = array(
		'en' => 'Access user and cart information for almost any part of SHOP.',
	);


	/**
	 * Returns a PluginDoc array that PyroCMS uses
	 * to build the reference in the admin panel
	 *
	 * All options are listed here but refer
	 * to the Asset plugin for a larger example
	 *
	 * @return array
	 */
	public function _self_doc()
	{
		$info = array(
			'product' => array(
				'description' => array(
					'en' => 'shop_discounts:product.'
				),
				'single' => false,
				'double' => true,
				'variables' => 'id',
				'attributes' => array(
					'id' => array(
						'type' => 'int',
						'required' => true,
					),

				),
			),

		);

		return $info;
	}


	/**
	 * Get discounts by product
	 */
	function product()
	{

		if($this->db->table_exists('shop_options'))
		{
			$x = explode(',', $this->attribute('x', '') );
			$id = intval( $this->attribute('id','0') );

			$this->load->model('shop_options/options_m');

			//$gid =($this->current_user)? intval($this->current_user->group_id): -1;

			//if(in_array('BYUSER', $x) && $gid > -1) $this->options_m->where('group_id',$gid);

			return (array) $this->options_m->getOptionsByProduct( $id );

		}

		return ''; //return blank
	}

	/**
	 * Autolmatically generates the HTML form options for a given product.
	 *
	 * {{shop_options:htmlproduct id="{{id}}" }}
	 *
	 * @return [type] [description]
	 */
	function htmlproduct()
	{
		if($this->db->table_exists('shop_options'))
		{
			$x = explode(',', $this->attribute('x', '') );
			$id = intval( $this->attribute('id','0') );

			$this->load->model('shop_options/options_m');
			$this->load->library('shop_options/Options_library');

			$options = $this->options_m->getOptionsByProduct( $id ) ;

			return $this->options_library->process($options);

		}

		return ''; //return blank
	}

}
/* End of file plugin.php */