<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * NitroCart    NitroCart.net - A full featured shopping cart system for PyroCMS
 *
 * @author      Salvatore Bordonaro
 * @version     2.2.0.2050
 * @website     http://nitrocart.net
 *              http://www.inspiredgroup.com.au
 *
 * @system      PyroCMS 2.2.x
 *
 */
class Options_library
{

    protected $option_prefix = 'option_';

    /**
     * Get the CI instance into this object
     *
     * @param unknown_type $var
     */
    public function __get($var)
    {
        if (isset(get_instance()->$var))
        {
            return get_instance()->$var;
        }
    }

    public function process($options, $divider='<br />')
    {
        $str = '';

        foreach($options as $key=>$option)
        {
            /*
            $str .= ''. $option->id;
            $str .= ''. $option->label;
            $str .= ''. $option->type;
            $str .= ''. $option->values;
            $str .= ''. $option->default;
            $str .= ''. $option->enabled;
            */
            switch($option->type)
            {
                case 'text':
                    $str .= $this->buildTextBox($option);
                    break;
                case 'select':
                    $str .= $this->buildSelect($option);
                    break;
                case 'radio':
                    $str .= $this->buildRadio($option);
                    break;
            }

            $str .= '<br />';
        }

        return $str;
    }

    /*
    public function get_key_value($option_id, $value_key)
    {
        $this->load->model('shop_options/options_m');

        $option = $this->options_m->get($option_id);

        if($option)
        {

        }


        return FALSE;
    }
    */

    private function buildTextBox($option)
    {
        return "{$option->label}<input type='text' name='{$this->option_prefix}{$option->id}' >";
    }

    private function buildSelect($option)
    {
        $str  = "<br/>{$option->label}<br/>";
        $str .= "<select name='{$this->option_prefix}{$option->id}'>";

        $v = explode(',',$option->values);

        foreach($v as $_option)
        {
            //list($value, $name) = explode('=', $_option);

            $str .= "<option value='{$_option}'>{$_option}</option> ";
        }

        $str .= "</select>";


        return $str;
    }

    private function buildRadio($option)
    {
        $str  = "<br/>{$option->label}<br/>";


        $v = explode(',',$option->values);

        foreach($v as $_option)
        {
            //list($value, $name) = explode('=', $_option);

            $str .= "<input type='radio' name='{$this->option_prefix}{$option->id}' value='{$_option}'> {$_option} <br />";
        }


        return $str;
    }

}